package davidminaya.asynctask;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * En este proyecto se explica como funciona la tarea asicronica
 * AsyncTask. Para explicar el funcionamiento de AsincTask se
 * crea una app que descarga y reproduce un audio haciendo uso
 * de esta tarea asicronica. Para utilizar la tarea asicronica
 * AsincTask es necesario crear una nueva clase que haga extent
 * de AsyncTask:
 *
 * @see davidminaya.asynctask.MainActivity.DescargarSonido
 *
 * Y lo implementamos creando una nueva instancia
 * de esa clase:
 *
 * new DescargarSonido().execute(url);
 *
 * En el metodo .execute(Parametros...) pasamos los parametros que
 * utilizara el metodo doInBackground para realizar la tarea
 * asicronica en la sub-clase de AsyncTask.
 *
 * En el proyecto se define lo siguiente:
 * -Clase MainActivity, es la clase principal del proyecto.
 *
 * -Clase DescargarSonido, hace extent de AsyncTask, esta es
 *  la clase en la que se implementa la tarea asicronica AsyncTask
 *  y en la que se descarga el audio que se reproducira.
 *
 * -Permiso INTERNET, definido en el manifest para tener acceso a
 *  internet y descargar el audio.
 *
 * -Permiso WRITE_EXTERNAL_STORAGE, definido en el manifest para
 *  acceder a la memoria del dispositivo y guardar el audio descargado.
 *
 * @author david minaya
 * */

public class MainActivity extends ActionBarActivity {

    private Button botonDescargar;
    private MediaPlayer mediaPlayer;
    private ProgressDialog progresoDialogo;
    public static final int progress_bar = 0;
    private static String url = "http://www.sonidosmp3gratis.com/sounds/las-carreras-de-caballos_1.mp3";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        botonDescargar = (Button) findViewById(R.id.descargar);

        botonDescargar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                botonDescargar.setEnabled(false);

                // Accede a la ubicacion del audio.
                File archivo = new File(Environment.getExternalStorageDirectory()
                        .getPath()+"/las-carreras-de-caballos_1.mp3");

                // En este if confirmamos si el archivo existe, si este existe
                // se ejecuta el metodo ReproducirSonido() que es el metodo que
                // se encarga de reproducir el audio que se descargo de
                // internet utilizando la tarea Asicronica AsyncTask. Si el
                // archivo no existe se ejecuta el AsyncTask para que descargue
                // el audio y luego lo reprodusca.
                if(archivo.exists()){

                    Toast.makeText(getApplicationContext(),
                            "El Archivo ya existe en la SD. Reproduciendo Sonido.",
                            Toast.LENGTH_SHORT).show();

                    // Metodo que reproduce el audio descargado.
                    ReproducirSonido();

                }else{

                    Toast.makeText(getApplicationContext(),
                            "El Archivo no Existe en la SD. Descargando.",
                            Toast.LENGTH_SHORT).show();

                    // Se crea una nueva instancia de la clase DescargarSonido
                    // en la cual se ejecuta la tarea asicronica AsyncTask.
                    // Utilizando el metodo .execute() a esta se le pasa el
                    // parametro con el cual estara ejecutando la tarea asicronica.
                    new DescargarSonido().execute(url);

                }
            }
        });

    }

   /**
    * En este metodo creamos el cuadro de dialogo de progreso que
    * nos mostrara el progreso de la tarea en proceso, en este caso
    * la descarga del audio.
    * */
    protected Dialog onCreateDialog(int id){
        switch (id){
            case progress_bar:
                progresoDialogo = new ProgressDialog(this);
                progresoDialogo.setMessage("Descargando Archivo Mp3. Espere...");
                progresoDialogo.setIndeterminate(false);
                progresoDialogo.setMax(100);
                progresoDialogo.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                progresoDialogo.setCancelable(false);
                progresoDialogo.show();
                return progresoDialogo;
            default:
                return null;
        }
    }

    /**
     * AsyncTask es una tarea asicronica que se ejecuta en segundo
     * plano y mantiene una comunicacion con la interfaz de usuario
     * para mostrar el progreso de la tarea que se este realizando.
     * Para utiliza AsyncTask es necesario declarar una clase que
     * haga extent de este. Al declarar la clase se tienen que
     * especificar dentro de los signos <...> el tipo de dato de los
     * parametros que utilizaran los metodos de la clase, estos tipos
     * de datos deben de ser de tipo objeto y no de tipo primitivo.
     * NOTA: "Aunque no lo paresca String no es un tipo de dato
     * primitivo".
     *
     * Ej:
     *    AsyncTask<String, Interger, Float>
     *
     *    Donde <String, , > define cual es el tipo de dato del
     *    parametro indefinido que recibe el metodo doInBackground
     *    para realizar la tarea.
     *
     *    < , Interger, > define el tipo de dato del parametro
     *    indefinido que se utilizara en el metodo onProgresUpdate
     *    para mostrar el progreso de la tarea que se este realizando.
     *
     *    < , , Float> define cual sera el tipo de dato del parametro
     *    que utiliza el metodo onPosExecute para retornara el
     *    resultado de la clase al terminar la tarea. Tambien define
     *    de cual tipo de dato sera el metodo doInBackground y su
     *    retorno.
     *
     *    AsyncTask<Parametro, Progreso, Resultado>
     *
     * No todos los tipos son siempre utilizados por una tarea
     * asíncrona. Para marcar un tipo como no utilizado,
     * simplemente utilice el tipo Void:
     * AsyncTask <Void, Void, Void> {...}
     *
     * Los metodos que utiliza AsyncTask para realizar la tarea son
     * los siguiente:
     *
     *  -onPreExecute, es el primer metodo en ejecutarce de la clase,
     *   se utiliza normalmente para iniciar la barra de progreso u
     *   otro componente de progreso. Este metodo es como el constructor
     *   de la clase.
     *
     *  -doInBackground, en este es el metodo se realiza la tarea de la
     *   clase y retorna el resultado de esta.
     *
     *  -onProgressUpdate, este metodo se comunica con la interfaz de
     *   usuario para mostrar el progreso de la tarea, este metodo
     *   recibe como parametro los valores que son enviados a travez
     *   de la sentencia "publishProgress()" desde el metodo
     *   doInBackground.
     *
     *   -onPostExecute, ultimo metodo en ejecutarce de la clase, este
     *   se utiliza para retornar el resultado de la tarea realizada,
     *   recibe como parametro el valor retornado por el metodo
     *   doInBackground.
     *
     * AsyncTask posee varios metodos de los cuales varios de estos
     * recibe parametros como es el caso del metodo
     * "doInBackground(String... archivo_url)", el parametro de este
     * metodo esta seguido por 3 puntos suspensivos (...) lo que
     * quiere decir que es un "Parametro indefenido", un parametro
     * indefinido puede recibir una cantidad infinita de parametros,
     * lo que quiere decir que a este metodo se le puede pasar la
     * cantidad que se desee de parametros para realizar la tarea.
     * En este ejemplo se muestra como funciona el "Parametro
     * indefinido".
     *
     * Ej:
     *
     *    // Llamamos el metodo y le pasamos 3 parametros
     *    Frutas("Mango", "Guineo", "Cereza");
     *
     *    // Creamos el metodo
     *    public void frutas (String... frutas){
     *        Log.i("Frutas",
     *             +"\n" +frutas[0]
     *             +"\n" +frutas[1]
     *             +"\n" +frutas[2]);
     *    }
     *
     * El ciclo de vida de AsyncTask es el siguiente:
     *
     * 1ro onPreExecute()
     * 2do doInBackground()
     * 3ro onProgressUpdate()
     * 4to onPosExecute()
     * */
    class DescargarSonido extends AsyncTask<String, String, String> {

        /**
         * Primer metodo en ejecutarse de la clase, en este metodo
         * se inicia el progressDialog.
         * */
        protected void onPreExecute(){
            super.onPreExecute();
            showDialog(progress_bar);
        }


        /**
         * Metodo que realiza la tarea asicronica con el parametro
         * pasado desde la sentencia "new DescargarSonido.execute(url);",
         * en este metodo se descarga el audio desde internet.
         * */
        @Override
        protected String doInBackground(String... archivo_url) {

            int count;

            try{
                // Recorre el parametro indefinido para obtener los
                // valores de los parametros que fueron pasados al
                // metodo y los captura en el atributo "url".
                URL url = new URL(archivo_url[0]);

                // Abre la conexion a internet
                URLConnection connection = url.openConnection();
                connection.connect();

                int lenghtOfFile = connection.getContentLength();

                // Abre el audio que se descagara
                InputStream input = new BufferedInputStream(url.openStream(),10*1024);

                // Abre la direccion donde se guardara el audio
                OutputStream output = new FileOutputStream(Environment
                        .getExternalStorageDirectory()
                        .getPath()+"/las-carreras-de-caballos_1.mp3"
                );

                byte data[] = new byte[1024];

                long total = 0;

                while((count = input.read(data)) != -1){

                    total += count;

                    // Envia el progreso de la tarea al metodo onProgressUpdate
                    // para mostrar al usuario el progreso de la tarea.
                    publishProgress(""+(int)((total * 100)/lenghtOfFile));

                    // Almacena el audio en memoria
                    output.write(data,0,count);
                }

                output.flush();
                output.close();
                input.close();

            } catch (IOException e) {

                Log.e("Error: ", e.getMessage());

            }

            // En este caso el metodo no retorna nada ya que el audio
            // se esta guardando aqui mismo.
            return null;
        }

        /**
         * Este metodo actualiza el progreso de la tarea que se esta
         * realizando, este metodo recibe como parametro los valores
         * que son enviados a travez de la sentencia "publishProgress()"
         * desde el metodo doInBackground.
         * */
        protected void onProgressUpdate(String... progress){

           // Actualiza el progreso de la descarga
           progresoDialogo.setProgress(Integer.parseInt(progress[0]));

        }

        /**
         * Este es el ultimo metodo de la tarea asicronica en
         * ejecutarse, recibe como parametro el valor retornado
         * por el metodo doInBackground.
         * */
        protected void onPostExecute(String archivo_url){

            //Cierra el cuadro de dialogo de progreso
            dismissDialog(progress_bar);


            Toast.makeText(getApplicationContext(),
                    "Descarga Completa. Reproduciendo audio.",
                    Toast.LENGTH_SHORT).show();

            // Reproduce el audio descargado
            ReproducirSonido();
        }
    }

    /**
     * Este metodo accede a la memoria del dispositivo para obtener el
     * audio descargado y reproducirlo.
     * */
    protected void ReproducirSonido(){

        // Ubicacion del audio en la memoria del dispositivo
        Uri uri = Uri.parse("file:///sdcard/las-carreras-de-caballos_1.mp3");

        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

        try{

            // Captura el audio almacenado
            mediaPlayer.setDataSource(getApplicationContext(), uri);

            // Prepara el audio para la reproduccion
            mediaPlayer.prepare();

            // Inicia la reproduccion
            mediaPlayer.start();

            // Evento que se ejecuta despues de haber finalizado la reproduccion
            // del audio
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {

                    botonDescargar.setEnabled(true);

                    Toast.makeText(getApplicationContext(),
                            "Archivo Reproducido Correctamente",
                            Toast.LENGTH_LONG).show();

                }

            });

        } catch (IOException e) {

            Toast.makeText(getApplicationContext(),
                    "IO Ocurrio un Error",
                    Toast.LENGTH_LONG).show();

        }catch (IllegalArgumentException e) {

            Toast.makeText(getApplicationContext(),
                    "Es psoible que no se establecio la URI correctamente",
                    Toast.LENGTH_LONG).show();

        }catch (SecurityException e) {

            Toast.makeText(getApplicationContext(),
                    "No se puede Acceder a la Uri, Se necesitan Permisos",
                    Toast.LENGTH_LONG).show();

        }catch (IllegalStateException e) {

            Toast.makeText(getApplicationContext(),
                    "Media Player no se encuentra en estado correcto",
                    Toast.LENGTH_LONG).show();

        }
    }
}

